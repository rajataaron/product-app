import React, {Component} from 'react';
import Category from './components/category';
import './index.css';

class App extends Component {
    render() {
        return (
            <Category category={this.state.category} />
        )
    }

    state = {
        category: []
    };

    componentDidMount() {
        fetch('https://backend.ustraa.com/rest/V1/api/homemenucategories/v1.0.1?device_type=mob')
            .then(res => res.json())
            .then((data) => {
                this.setState({ category: data.category_list })
            })
            .catch(console.log)
    }
}

export default App;
