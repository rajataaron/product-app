import React from 'react'

const Category = ({category}) => {
    return (
        <div>
            <center><h1>Contact List</h1></center>
            {category.map((category) => (
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{category.category_id}</h5>
                        <h5 class="card-title">{category.category_name}</h5>
                        <img src={category.category_image}></img>
                    </div>
                </div>
            ))}
        </div>
    )
};


export default Category